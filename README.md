Python script for extracting website visitor statistics from nginx logfiles
===========================================================================

This is a very simple script using the **ipwhois** module for querying IP
addresses of visitors. See the code for more details. 

License
=======

Please feel free to use the content of this repo without any warranty,
according to the GNU GPLv3 license.

https://www.gnu.org/licenses/gpl-3.0.en.html
